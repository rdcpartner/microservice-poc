package com.rdcpartner.customer.exception;

public class CustomerNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1189458018195889049L;
}
