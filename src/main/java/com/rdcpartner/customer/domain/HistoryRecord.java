package com.rdcpartner.customer.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "history_index", type = "history")
public class HistoryRecord {

    @Id
    private String id;

    private String recordType;

    private String recordId;

    private Date createDate = new Date();

    private List<PropertyHistoryRecord> difference = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public List<PropertyHistoryRecord> getDifference() {
        return difference;
    }

    public void setDifference(List<PropertyHistoryRecord> difference) {
        this.difference = difference;
    }

    @Override
    public String toString() {
        return "HistoryRecord [id=" + id + ", recordType=" + recordType + ", recordId=" + recordId + ", createDate="
                + createDate + ", difference=" + difference + "]";
    }
}
