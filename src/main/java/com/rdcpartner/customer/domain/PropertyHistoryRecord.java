package com.rdcpartner.customer.domain;

public class PropertyHistoryRecord {

    private String status;

    private String name;

    private String oldValue;

    private String newValue;

    public PropertyHistoryRecord() {
        // no op
    }

    public PropertyHistoryRecord(String status, String name, String oldValue, String newValue) {
        this.status = status;
        this.name = name;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    @Override
    public String toString() {
        return "PropertyHistoryRecord [status=" + status + ", name=" + name + ", oldValue=" + oldValue + ", newValue="
                + newValue + "]";
    }
}
