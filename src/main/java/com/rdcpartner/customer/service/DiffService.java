package com.rdcpartner.customer.service;

import static java.lang.String.valueOf;
import static org.codegeny.beans.model.Model.STRING;
import static org.codegeny.beans.model.Model.bean;
import static org.codegeny.beans.model.Model.property;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.codegeny.beans.diff.Diff;
import org.codegeny.beans.diff.Diff.Status;
import org.codegeny.beans.diff.SimpleDiff;
import org.codegeny.beans.model.BeanModel;
import org.codegeny.beans.model.visitor.diff.ComputeDiffModelVisitor;
import org.springframework.stereotype.Component;

import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.domain.PropertyHistoryRecord;
import com.rdcpartner.customer.domain.HistoryRecord;

@Component
public class DiffService {

    private final double threshold = 1.0D;

    private BeanModel<Customer> customerBeanModel;

    @PostConstruct
    public void init() {
        customerBeanModel = bean(Customer.class,
                                    property("firstName", Customer::getFirstName, STRING),
                                    property("lastName", Customer::getLastName, STRING));
    }

    public HistoryRecord diff(Customer oldCustomer, Customer newCustomer) {
        ComputeDiffModelVisitor<Customer, Customer> visitor = new ComputeDiffModelVisitor<>(oldCustomer, newCustomer, threshold);
        Diff<Customer> diff = customerBeanModel.accept(visitor);
        HistoryRecord record = new HistoryRecord();
        record.setRecordType(Customer.class.getSimpleName());
        List<PropertyHistoryRecord> list = diff(diff);
        record.setDifference(list);
        return record;
    }

    public List<PropertyHistoryRecord> diff(Diff<?> diff) {
        List<PropertyHistoryRecord> list = new ArrayList<>();
        diff.traverse((p, u) -> {
            if (u instanceof SimpleDiff<?>) {
                SimpleDiff<?> sd = (SimpleDiff<?>) u;
                Status status = sd.getStatus();
                String name = p.toString().substring(1); // remove leading /
                PropertyHistoryRecord history = new PropertyHistoryRecord(status.name(),
                                                name,
                                                sd.getLeft() != null ? valueOf(sd.getLeft()) : null,
                                                sd.getRight() != null ? valueOf(sd.getRight()) : null);
                list.add(history);
            }
        });
        return list;
    }
}
