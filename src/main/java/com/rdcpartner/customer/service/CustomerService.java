package com.rdcpartner.customer.service;

import static org.elasticsearch.index.query.QueryBuilders.commonTermsQuery;
import static org.elasticsearch.search.sort.SortOrder.DESC;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.elasticsearch.index.query.CommonTermsQueryBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.domain.HistoryRecord;
import com.rdcpartner.customer.exception.CustomerNotFoundException;
import com.rdcpartner.customer.repository.CustomerMongoRepository;
import com.rdcpartner.customer.repository.HistoryElasticRepository;

@Service
public class CustomerService {

    @Autowired
    private CustomerMongoRepository customerMongoRepository;

    @Autowired
    private HistoryElasticRepository elasticsearchRepository;

    @Autowired
    private DiffService diffService;

    public Customer save(Customer customer) {
        boolean isNew = customer.getId() == null;
        Customer oldCustomer = null;
        Customer inserted = null;
        if (isNew) {
            inserted = customerMongoRepository.insert(customer);
        } else {
            Optional<Customer> found = customerMongoRepository.findById(customer.getId());
            if (found.isPresent()) {
                oldCustomer = found.get();
                inserted = customerMongoRepository.save(customer);
            } else {
                throw new CustomerNotFoundException();
            }
        }
        HistoryRecord hr = diffService.diff(oldCustomer, customer);
        hr.setRecordId(inserted.getId());
        elasticsearchRepository.save(hr);
        return inserted;
    }


    public Optional<Customer> get(String customerId) {
        return customerMongoRepository.findById(customerId);
    }

    public List<HistoryRecord> getHistory(String customerId) {
        CommonTermsQueryBuilder query = commonTermsQuery("recordId", customerId);
        NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder().withQuery(query);
        searchQuery.withSort(SortBuilders.fieldSort("createDate").order(DESC));
        Iterable<HistoryRecord> search = elasticsearchRepository.search(searchQuery.build());
        List<HistoryRecord> list = new ArrayList<>();
        search.forEach(list::add);
        return list;
    }


    public List<Customer> listCustomers() {
        return customerMongoRepository.findAll();
    }
}
