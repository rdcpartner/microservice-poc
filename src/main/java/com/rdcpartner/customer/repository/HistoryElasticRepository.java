package com.rdcpartner.customer.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.rdcpartner.customer.domain.HistoryRecord;

@Repository
public interface HistoryElasticRepository extends ElasticsearchRepository<HistoryRecord, String> {

}
