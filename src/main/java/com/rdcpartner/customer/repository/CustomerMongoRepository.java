package com.rdcpartner.customer.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rdcpartner.customer.domain.Customer;

@Repository
public interface CustomerMongoRepository extends MongoRepository<Customer, String> {

}
