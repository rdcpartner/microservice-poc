package com.rdcpartner.customer.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.domain.HistoryRecord;
import com.rdcpartner.customer.exception.CustomerNotFoundException;
import com.rdcpartner.customer.service.CustomerService;

@Controller
@Path("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @POST
    @Path("/")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Customer create(Customer customer) {
        try {
            return customerService.save(customer);
        } catch (CustomerNotFoundException e) {
            throw new WebApplicationException("Customer not found", NOT_FOUND);
        }
    }

    @PUT
    @Path("/{id}")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Customer update(@PathParam("id") String id, Customer customer) {
        try {
        	customer.setId(id);
            return customerService.save(customer);
        } catch (CustomerNotFoundException e) {
            throw new WebApplicationException("Customer not found", NOT_FOUND);
        }
    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("/{customerId}")
    public Customer get(@PathParam("customerId") String customerId) {
        Optional<Customer> customer = customerService.get(customerId);
        if (customer.isPresent()) {
            return customer.get();
        } else {
            throw new WebApplicationException("Customer not found", NOT_FOUND);
        }
    }

    @GET
    @Path("/history/{customerId}")
    @Produces(APPLICATION_JSON)
    public List<HistoryRecord> getHistory(@PathParam("customerId") String customerId) {
        return customerService.getHistory(customerId);
    }

    @GET
    @Path("/")
    @Produces(APPLICATION_JSON)
    public List<Customer> listCustomers() {
        return customerService.listCustomers();
    }
}
