package com.rdcpartner.customer;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.domain.HistoryRecord;

import io.restassured.mapper.TypeRef;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
public class CustomerControllerTest extends AbstractCustomerTest {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }
    
    @Test
    public void testInsertCustomer() {
        Customer customer = new Customer();
        customer.setFirstName("name-" + new Random().nextInt());
        customer.setLastName("lastname-" + new Random().nextInt());
        Response response = given()
                            .contentType(JSON)
                            .body(customer)
                            .post("/customers")
                            .then()
                            .statusCode(200)
                            .extract()
                            .response();

        Customer inserted = response.as(Customer.class);
        assertEquals(customer.getFirstName(), inserted.getFirstName());
        assertEquals(customer.getLastName(), inserted.getLastName());
        assertNotNull(inserted.getId());

        customer.setFirstName(inserted.getLastName());
        customer.setLastName(inserted.getFirstName());
        customer.setId(null);

        response = given()
                    .contentType(JSON)
                    .body(customer)
                    .put("/customers/" + inserted.getId())
                    .then()
                    .statusCode(200)
                    .extract()
                    .response();

        Customer updated = response.as(Customer.class);
        assertEquals(updated.getFirstName(), inserted.getLastName());
        assertEquals(updated.getLastName(), inserted.getFirstName());
        assertNotNull(updated.getId());
        assertEquals(inserted.getId(), updated.getId());

        Response historyResponse = given()
                                    .get("/customers/history/" + inserted.getId())
                                    .then()
                                    .statusCode(200)
                                    .extract()
                                    .response();

        List<HistoryRecord> history = historyResponse.as(new TypeRef<List<HistoryRecord>>() {});
        assertNotNull(history);
        assertEquals(2, history.size());
        assertEquals("Customer", history.get(0).getRecordType());

        Response getResponse = given()
                                .get("/customers/" + updated.getId())
                                .then()
                                .statusCode(200)
                                .extract()
                                .response();

        customer = getResponse.as(Customer.class);
        assertEquals(updated.getId(), customer.getId());

        Response responseCustomerList = given()
                                            .get("/customers")
                                            .then()
                                            .statusCode(200)
                                            .extract()
                                            .response();

        List<Customer> customers = responseCustomerList.as(new TypeRef<List<Customer>>() { });
        assertNotNull(customers);
        assertTrue(customers.size() > 0);
    }
}
