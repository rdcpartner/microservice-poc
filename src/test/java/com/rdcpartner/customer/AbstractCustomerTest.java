package com.rdcpartner.customer;

import java.io.IOException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;

public abstract class AbstractCustomerTest {

    private static EmbeddedElastic elastic;

    @BeforeAll
    public static void beforeAll() throws IOException, InterruptedException {
        elastic = EmbeddedElastic
                            .builder()
                            .withElasticVersion("6.8.6")
                            .build()
                            .start();
    }

    @AfterAll
    public static void afterAll() {
        if ( elastic != null) {
            elastic.stop();
        }
    }
}
