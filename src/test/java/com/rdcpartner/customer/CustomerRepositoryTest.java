package com.rdcpartner.customer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mongodb.client.MongoDatabase;
import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.repository.CustomerMongoRepository;

import io.fares.junit.mongodb.MongoExtension;
import io.fares.junit.mongodb.MongoForAllExtension;

@SpringBootTest
class CustomerRepositoryTest extends AbstractCustomerTest {

    @RegisterExtension
    static MongoForAllExtension mongo = MongoForAllExtension.defaultMongo();

    @Autowired
    private CustomerMongoRepository customerRepository;
    
    @BeforeEach
    void setupCollection() {
        MongoDatabase db = mongo.getMongoClient().getDatabase(MongoExtension.UNIT_TEST_DB);
        db.getCollection("CustomerCollection").drop();
    }

    @Test
    public void testCreate() {
        Customer customer = new Customer();
        customer.setFirstName("foo");
        customer.setLastName("bar");
        customer = customerRepository.insert(customer);
        assertNotNull(customer.getId(), "Customer id shouldn't be null");
    }
}
