package com.rdcpartner.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.rdcpartner.customer.domain.Customer;
import com.rdcpartner.customer.domain.PropertyHistoryRecord;
import com.rdcpartner.customer.domain.HistoryRecord;
import com.rdcpartner.customer.service.DiffService;

@SpringBootTest(webEnvironment = WebEnvironment.NONE)
public class DiffServiceTest extends AbstractCustomerTest {

    @Autowired
    private DiffService diffService;

    @Test
    public void test() {
        Customer customer = new Customer();
        customer.setFirstName("foo");
        customer.setLastName("bar");
        HistoryRecord hr = diffService.diff(null, customer);
        List<PropertyHistoryRecord> list = hr.getDifference();
        assertEquals("Customer", hr.getRecordType());
        assertEquals(2, list.size());
        assertEquals("foo", list.get(0).getNewValue());
        assertEquals("bar", list.get(1).getNewValue());
        assertEquals(null, list.get(0).getOldValue());
        assertEquals(null, list.get(1).getOldValue());
    }
}
